# Faire une recherche dans Mobilizon

!!! info
    La recherche se fait&nbsp;:

      * pour les événements&nbsp;: sur les titres, les tags, et les descriptions
      * pour les groupes&nbsp;: seulement sur les noms de groupes (pour le moment)

Une recherche rapide est possible depuis le champ **Rechercher** dans la barre de navigation supérieure du site&nbsp;:

![image barre de recherches menu supérieur](../../images/search-bar-top-FR.png)

Vous pouvez directement aller dans la recherche avancée en cliquant sur **Explorer** dans la barre de navigation supérieure du site. Les options disponibles sont&nbsp;:

  * **Mots-clés**&nbsp;: tags ou mots présents dans le titre et/ou la description
  * **Lieu**&nbsp;: pour filtrer les événements dans un lieu précis
  * **Rayon**&nbsp;: pour élargir les résultats du lieu à une certaine distance
  * **Date**&nbsp;: pour filtrer les événements en fonction de quand ils auront lieu (non disponible pour une recherche de groupe)

![image montrant les menus déroulant pour lieu et date](../../images/search-radius-where-FR.gif)

## Exemple

Si vous souhaitez, par exemple, trouver un événement de collage, vous entrerez `collage` dans la recherche&nbsp;:

![image de recherche de collage](../../images/search-collage-no-filter-FR.png)

Et si vous souhaitez limiter la recherche à Lyon et alentours, vous entrerez `Lyon` dans **Lieu**&nbsp;:

![img collage lieu filtre](../../images/search-collage-location-filter-FR.png)

Ou, pour trouver un **groupe** [Contribatelier](https://contribateliers.org/) en cliquant sur l'onglet **Groupes**&nbsp;:

![image recherche groupe](../../images/search-group-contrib-FR.png)
